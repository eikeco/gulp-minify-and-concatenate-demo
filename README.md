# Gulp Minify And Concatenate Demo

An example project using Gulp to minify and concatenate files.

# Setup

- Clone repository:
<code>git clone git@github.com:eikeco/gulp-minify-and-concatenate-demo.git</code>

- Navigation into the project:
<code>cd gulp-minify-and-concatenate-demo</code>

- Install dependancies:
<code>npm install</code>

- Build the project:
<code>gulp</code>

# Issues

[Raise them here](https://github.com/eikeco/gulp-minify-and-concatenate-demo/issues)
