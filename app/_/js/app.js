// = Some test code
//---------------------------------------------------------------------------//

;(function(document) {
    "use strict";

    // get some json data
    var request = new XMLHttpRequest();
    request.open('GET', 'http://jsonplaceholder.typicode.com/posts/1', true);

    // on load
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            // Success!
            var data = JSON.parse(request.responseText),
                bar = document.getElementById('bar');

            console.log(data);
            bar.innerHTML = '<h2>' + data.title + '</h2>' + '<p>' + data.body + '</p>';

        } else {
            // We reached our target server, but it returned an error
            alert('Sorry, there\'s been an error');
        }
    };

    request.onerror = function () {
        // There was a connection error of some sort
    };

    request.send();

})(document);
