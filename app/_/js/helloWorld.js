// = Hello World
//---------------------------------------------------------------------------//

;(function(document) {
    "use strict";

    var foo = document.getElementById('foo');

    // classic
    foo.insertAdjacentHTML('afterbegin', '<h1>Hello World<h1>');

})(document);
