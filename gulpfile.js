// = require packages
//---------------------------------------------------------------------------//

    var gulp = require('gulp'),
        sass = require('gulp-sass'),
        uglify = require('gulp-uglify'),
        useref = require('gulp-useref');


// = tasks
//---------------------------------------------------------------------------//

    gulp.task('clean', require('del').bind(null, ['dist']));

    gulp.task('default', ['clean'], function () {

        var assets = useref.assets();

        return gulp.src('app/index.html')
        .pipe(assets)
        .pipe(uglify())
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('dist'));

    });
